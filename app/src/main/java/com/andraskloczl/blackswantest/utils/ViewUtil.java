package com.andraskloczl.blackswantest.utils;

import android.view.View;

/**
 * Created by andraskloczl on 05/07/16.
 */
public class ViewUtil {

    public static void visible(View... views) {
        for (View view : views) {
            setVisibility(view, View.VISIBLE);
        }
    }

    public static void invisible(View... views) {
        for (View view : views) {
            setVisibility(view, View.INVISIBLE);
        }
    }

    public static void gone(View... views) {
        for (View view : views) {
            setVisibility(view, View.GONE);
        }
    }

    public static void setVisibility(View view, boolean visibility) {
        if (view == null) {
            return;
        }

        if ((visibility && !isVisible(view)) || (!visibility)) {
            if (visibility) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        }
    }

    public static void setVisibility(View view, int visibility) {
        if (view == null) {
            return;
        }

        int currentVisibility = view.getVisibility();
        if (currentVisibility != visibility) {
            view.setVisibility(visibility);
        }
    }

    public static boolean isVisible(View view) {
        return view != null && view.getVisibility() == View.VISIBLE;
    }


}
