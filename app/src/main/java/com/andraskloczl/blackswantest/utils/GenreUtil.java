package com.andraskloczl.blackswantest.utils;

import com.andraskloczl.blackswantest.network.models.genre.Genre;

import java.util.List;

/**
 * Created by andraskloczl on 04/07/16.
 */
public class GenreUtil {

    private static final String COMMA = ", ";

    public static String getGenresString(List<Genre> genres) {
        if(genres == null) return "";

        return android.text.TextUtils.join(COMMA, genres);
    }
}
