package com.andraskloczl.blackswantest.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.andraskloczl.blackswantest.R;

/**
 * Created by andraskloczl on 05/07/16.
 */
public class DialogUtil {

    public static AlertDialog showDialog(Context context, String dialogText, String buttonText,
                                         DialogInterface.OnClickListener onClickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(dialogText);
        builder.setCancelable(false);

        builder.setPositiveButton(buttonText, onClickListener);

        AlertDialog alert = builder.create();
        alert.show();

        return alert;
    }

    public static AlertDialog showDialog(Context context, String dialogText, String buttonText) {
        return showDialog(context, dialogText, buttonText, null);
    }

    public static AlertDialog showDialog(Context context, String dialogText) {
        return showDialog(context, dialogText, context.getString(R.string.ok));
    }

    public static AlertDialog showDialog(Context context, String dialogText, String positiveButtonText,
                                         String negativeButtonText,
                                         DialogInterface.OnClickListener positiveButtonOnclickListener,
                                         DialogInterface.OnClickListener negativeButtonOnclickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(dialogText);
        builder.setCancelable(true);

        builder.setPositiveButton(positiveButtonText, positiveButtonOnclickListener);
        builder.setNegativeButton(negativeButtonText, negativeButtonOnclickListener);

        AlertDialog alert = builder.create();
        alert.show();

        return alert;
    }

    public static ProgressDialog showProgressDialog(Context context, String title, String message) {
        ProgressDialog progressDialog = ProgressDialog.show(context, title, message, true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        return progressDialog;
    }

}
