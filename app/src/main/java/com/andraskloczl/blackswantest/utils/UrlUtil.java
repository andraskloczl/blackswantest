package com.andraskloczl.blackswantest.utils;

import com.andraskloczl.blackswantest.BuildConfig;

/**
 * Created by andraskloczl on 04/07/16.
 */
public class UrlUtil {

    public static final String MOVIE_DB_API_KEY = BuildConfig.MOVIE_DB_API_KEY;
    public static final String IMAGE_BASE_URL = BuildConfig.MOVIE_DB_IMAGE_BASE_URL;
    public static final String MOVIE_DB_BASE_URL = BuildConfig.MOVIE_DB_BASE_URL;

    public static String getImageUrl(String path) {
        return IMAGE_BASE_URL + path;
    }
}
