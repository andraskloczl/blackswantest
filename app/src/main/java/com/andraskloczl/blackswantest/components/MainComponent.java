package com.andraskloczl.blackswantest.components;

import com.andraskloczl.blackswantest.modules.MainModule;
import com.andraskloczl.blackswantest.pages.movies.MoviesMenuFragment;
import com.andraskloczl.blackswantest.pages.tvseries.TvSeriesMenuFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by andraskloczl on 05/07/16.
 */

@Singleton
@Component(modules = {MainModule.class})
public interface MainComponent {
    void inject(MoviesMenuFragment moviesMenuFragment);
    void inject(TvSeriesMenuFragment moviesMenuFragment);
}
