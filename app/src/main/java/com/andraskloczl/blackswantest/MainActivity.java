package com.andraskloczl.blackswantest;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.andraskloczl.blackswantest.pages.NavigationDrawer;
import com.andraskloczl.blackswantest.pages.movies.MoviesMenuFragment;
import com.andraskloczl.blackswantest.pages.tvseries.TvSeriesMenuFragment;
import com.andraskloczl.blackswantest.utils.ConnectionUtil;
import com.andraskloczl.blackswantest.utils.DialogUtil;
import com.andraskloczl.blackswantest.utils.ViewUtil;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";

    private View loadingView;
    private View noConnectionView;
    private TextView noConnectionViewLabel;
    private View retryButton;

    private NavigationDrawer navigationDrawer;
    private Toolbar toolbar;
    private TextView toolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        loadingView = findViewById(R.id.activity_main_loadingview);
        noConnectionView = findViewById(R.id.activity_main_no_connection_view);
        noConnectionViewLabel = (TextView) findViewById(R.id.activity_main_no_connection_view_label);
        retryButton = findViewById(R.id.activity_main_no_connection_view_retry_button);
        retryButton.setOnClickListener(this);

        initToolbar();
        setupNavigationMenu();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.blackswan_toolbar);
        if(toolbar != null) {
            setSupportActionBar(toolbar);
        }

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        toolbarTitle = (TextView) findViewById(R.id.blackswan_toolbar_title);
    }

    private void setupNavigationMenu() {
        final DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ListView drawerItemsListView = (ListView) findViewById(R.id.nav_drawer_items_listview);

        navigationDrawer = new NavigationDrawer(this, toolbar, drawerLayout, drawerItemsListView,
                R.id.main_fragment_container);

        MoviesMenuFragment moviesMenuFragment = new MoviesMenuFragment();
        navigationDrawer.addNewMenuFragment(moviesMenuFragment);

        TvSeriesMenuFragment tvSeriesMenuFragment = new TvSeriesMenuFragment();
        navigationDrawer.addNewMenuFragment(tvSeriesMenuFragment);

        navigationDrawer.showMenuFragment(moviesMenuFragment);
    }

    @Override
    public void onBackPressed() {
        if (navigationDrawer.isDrawerOpen()) {
            navigationDrawer.closeDrawer();
            return;
        }

        if(!navigationDrawer.isFirstPageVisible()) {
            navigationDrawer.goToFirstPage();
            return;
        }

        DialogUtil.showDialog(this, getString(R.string.are_you_sure_you_want_to_quit),
                getString(R.string.quit), getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );
    }

    public void setVisibilityForLoadingView(boolean visible) {
        ViewUtil.setVisibility(loadingView, visible);
    }

    public void setToolbarTitle(String title) {
        toolbarTitle.setText(title);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_main_no_connection_view_retry_button:
                if(ConnectionUtil.isConnected(this)) {
                    navigationDrawer.getVisibleFragment().reload();
                    ViewUtil.gone(noConnectionView);
                } else {
                    Toast.makeText(MainActivity.this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    public void onNetworkError() {
        ViewUtil.visible(noConnectionView);
        if(ConnectionUtil.isConnected(this)) {
            noConnectionViewLabel.setText(getString(R.string.oops_something_went_wrong));
        } else {
            noConnectionViewLabel.setText(getString(R.string.no_internet));
        }
    }
}
