package com.andraskloczl.blackswantest.modules;

import com.andraskloczl.blackswantest.moviedb.MovieDbManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by andraskloczl on 05/07/16.
 */
@Module
public class MainModule {

    @Provides
    @Singleton
    MovieDbManager provideMovieDbManager() {
        return new MovieDbManager();
    }
}
