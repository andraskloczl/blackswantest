package com.andraskloczl.blackswantest;

import android.app.Application;

import com.andraskloczl.blackswantest.components.DaggerMainComponent;
import com.andraskloczl.blackswantest.components.MainComponent;
import com.andraskloczl.blackswantest.modules.MainModule;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

/**
 * Created by andraskloczl on 04/07/16.
 */
public class BlackSwanApplication extends Application {

    private MainComponent mainComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mainComponent = DaggerMainComponent.builder()
                .mainModule(new MainModule())
                .build();

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
//        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
    }

    public MainComponent getMainComponent() {
        return mainComponent;
    }
}
