package com.andraskloczl.blackswantest.network.models.genre;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * Created by andraskloczl on 04/07/16.
 */

@Data
public class FetchGenresResponse implements Serializable {

    @SerializedName("genres")
    private List<Genre> genres;
}
