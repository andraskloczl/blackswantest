package com.andraskloczl.blackswantest.network;

import com.andraskloczl.blackswantest.network.models.genre.FetchGenresResponse;
import com.andraskloczl.blackswantest.network.models.movie.FetchPopularMoviesResponse;
import com.andraskloczl.blackswantest.network.models.tv.FetchPopularTvSeriesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by andraskloczl on 04/07/16.
 */
public interface MovieDbRepository {

    @GET("movie/popular")
    Call<FetchPopularMoviesResponse> getPopularMovies(@Query("api_key") String apiKey,
                                                      @Query("page") int page);

    @GET("tv/popular")
    Call<FetchPopularTvSeriesResponse> getPopularTvSeries(@Query("api_key") String apiKey,
                                                          @Query("page") int page);

    @GET("genre/movie/list")
    Call<FetchGenresResponse> getMovieGenres(@Query("api_key") String apiKey);

    @GET("genre/tv/list")
    Call<FetchGenresResponse> getTvSeriesGenres(@Query("api_key") String apiKey);
}
