package com.andraskloczl.blackswantest.network.models.genre;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;

/**
 * Created by andraskloczl on 04/07/16.
 */
@Data
public class Genre implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @Override
    public String toString() {
        return name;
    }
}
