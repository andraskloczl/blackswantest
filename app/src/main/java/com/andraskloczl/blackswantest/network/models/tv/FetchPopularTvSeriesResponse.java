package com.andraskloczl.blackswantest.network.models.tv;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * Created by andraskloczl on 05/07/16.
 */
@Data
public class FetchPopularTvSeriesResponse implements Serializable {

    @SerializedName("page")
    private int page;

    @SerializedName("results")
    private List<TvSeries> tvSeries;

    @SerializedName("total_results")
    private int totalResults;

    @SerializedName("total_pages")
    private int totalPages;

}

