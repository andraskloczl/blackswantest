package com.andraskloczl.blackswantest.network;

public interface NetworkCallback<T> {
    void onSuccess(T t);
    void onError();
}
