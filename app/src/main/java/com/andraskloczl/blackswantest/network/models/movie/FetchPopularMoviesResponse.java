package com.andraskloczl.blackswantest.network.models.movie;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * Created by andraskloczl on 04/07/16.
 */

@Data
public class FetchPopularMoviesResponse implements Serializable {

    @SerializedName("page")
    private int page;

    @SerializedName("results")
    private List<Movie> movies;

    @SerializedName("total_results")
    private int totalResults;

    @SerializedName("total_pages")
    private int totalPages;

}
