package com.andraskloczl.blackswantest.network;

import android.util.Log;

import java.util.concurrent.Callable;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class CallableExecutor {

    public static synchronized <T> void executeCallable(Callable<T> callable) {
        executeCallable(callable, null);
    }

    public static synchronized <T> void executeCallable(Callable<T> callable, final NetworkCallback<T> callback) {

        try {
            Observable.fromCallable(callable)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<T>() {
                        @Override
                        public void call(T data) {
                            Log.d("CallableExecutor", "- DONE");
                            if(callback != null) {
                                callback.onSuccess(data);
                            }
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.e("CallableExecutor"," - ERROR: " + throwable.getMessage());
                            if(callback != null) {
                                callback.onError();
                            }
                        }
                    });
        } catch (Exception ex) {
            Log.d("CallableExecutor", ex.getMessage());
            callback.onError();
        }
    }
}
