package com.andraskloczl.blackswantest.moviedb;

import android.content.Context;
import android.util.Log;

import com.andraskloczl.blackswantest.network.CallableExecutor;
import com.andraskloczl.blackswantest.network.MovieDbRepository;
import com.andraskloczl.blackswantest.network.NetworkCallback;
import com.andraskloczl.blackswantest.network.models.genre.FetchGenresResponse;
import com.andraskloczl.blackswantest.network.models.genre.Genre;
import com.andraskloczl.blackswantest.network.models.movie.FetchPopularMoviesResponse;
import com.andraskloczl.blackswantest.network.models.movie.Movie;
import com.andraskloczl.blackswantest.network.models.tv.FetchPopularTvSeriesResponse;
import com.andraskloczl.blackswantest.network.models.tv.TvSeries;
import com.andraskloczl.blackswantest.utils.ConnectionUtil;
import com.andraskloczl.blackswantest.utils.UrlUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import javax.inject.Inject;

import lombok.Getter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andraskloczl on 04/07/16.
 */

public class MovieDbManager {

    private static final String TAG = "MovieDbManager";
    private static final int COUNT_TO_LOAD = 20;

    @Getter
    private List<Movie> movieList;
    @Getter
    private List<TvSeries> tvSeriesList;

    @Getter
    private volatile int lastLoadedMoviePage = 0;
    @Getter
    private volatile int lastLoadedTvSeriesPage = 0;

    @Inject
    public MovieDbManager() {
        init();
    }

    private Retrofit retrofit;
    private MovieDbRepository repository;

    private CountDownLatch fetchMovieCountDownLatch;
    private CountDownLatch fetchTvSeriesCountDownLatch;

    @Getter
    private Map<Integer, Genre> movieGenres;
    private Map<Integer, Genre> tvSeriesGenres;

    private void init() {
        retrofit = new Retrofit.Builder()
                .baseUrl(UrlUtil.MOVIE_DB_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        repository = retrofit.create(MovieDbRepository.class);

        movieList = new ArrayList<>();
        tvSeriesList = new ArrayList<>();
        movieGenres = new HashMap<>();
        tvSeriesGenres = new HashMap<>();
    }

    private void fetchMovieGenres(final NetworkCallback<FetchPopularMoviesResponse> callback) {
        CallableExecutor.executeCallable(
                new Callable<FetchGenresResponse>() {
                    @Override
                    public FetchGenresResponse call() throws Exception {
                        Response<FetchGenresResponse> response =
                                repository.getMovieGenres(UrlUtil.MOVIE_DB_API_KEY).execute();
                        return response.body();
                    }
                },
                new NetworkCallback<FetchGenresResponse>() {
                    @Override
                    public void onSuccess(FetchGenresResponse response) {
                        Log.d(TAG, "FetchGenres - onSuccess");

                        for(Genre genre : response.getGenres()) {
                            movieGenres.put(genre.getId(), genre);
                        }

                        fetchMovieCountDownLatch.countDown();
                    }

                    @Override
                    public void onError() {
                        Log.e(TAG, "FetchGenres - onError");
                        callback.onError();
                        fetchMovieCountDownLatch.countDown();
                    }
                });

    }

    public void fetchPopularMovies(final Context context, final int page, final NetworkCallback<FetchPopularMoviesResponse> callback) {
        if(!ConnectionUtil.isConnected(context)) {
            callback.onError();
            return;
        }
        if(movieGenres.isEmpty()) {
            fetchMovieCountDownLatch = new CountDownLatch(1);
            fetchMovieGenres(callback);
        }

        CallableExecutor.executeCallable(
                new Callable<FetchPopularMoviesResponse>() {
                    @Override
                    public FetchPopularMoviesResponse call() throws Exception {
                        fetchMovieCountDownLatch.await();
                        Response<FetchPopularMoviesResponse> response =
                                repository.getPopularMovies(UrlUtil.MOVIE_DB_API_KEY, page).execute();
                        return response.body();
                    }
                },
                new NetworkCallback<FetchPopularMoviesResponse>() {
                    @Override
                    public void onSuccess(FetchPopularMoviesResponse fetchPopularMoviesResponse) {
                        lastLoadedMoviePage = page;

                        fillGenresToMovies(fetchPopularMoviesResponse.getMovies());
                        movieList.addAll(fetchPopularMoviesResponse.getMovies());

                        callback.onSuccess(fetchPopularMoviesResponse);

                        if(movieList.size() < COUNT_TO_LOAD) {
                            fetchPopularMovies(context, lastLoadedMoviePage + 1, callback);
                        }

                        Log.d(TAG, "Loaded " + fetchPopularMoviesResponse.getMovies().size() +
                                " movies, last loaded page is: " + lastLoadedMoviePage);
                    }

                    @Override
                    public void onError() {
                        callback.onError();
                        Log.d(TAG, "fetchPopularMovies - onError");
                    }
                });
    }

    private void fillGenresToMovies(List<Movie> movieList) {
        for(Movie movie : movieList) {
            if(movie.getGenreIdList() == null) return;

            List<Genre> newGenreList = new ArrayList<>();

            for(Integer genreId : movie.getGenreIdList()) {
                newGenreList.add(movieGenres.get(genreId));
            }
            movie.setGenres(newGenreList);
        }
    }

    private void fetchTvSeriesGenres(final NetworkCallback<FetchPopularTvSeriesResponse> callback) {
        CallableExecutor.executeCallable(
                new Callable<FetchGenresResponse>() {
                    @Override
                    public FetchGenresResponse call() throws Exception {
                        Response<FetchGenresResponse> response =
                                repository.getTvSeriesGenres(UrlUtil.MOVIE_DB_API_KEY).execute();
                        return response.body();
                    }
                },
                new NetworkCallback<FetchGenresResponse>() {
                    @Override
                    public void onSuccess(FetchGenresResponse response) {
                        Log.d(TAG, "FetchGenres - onSuccess");

                        for(Genre genre : response.getGenres()) {
                            tvSeriesGenres.put(genre.getId(), genre);
                        }

                        fetchTvSeriesCountDownLatch.countDown();
                    }

                    @Override
                    public void onError() {
                        Log.e(TAG, "FetchGenres - onError");
                        callback.onError();
                        fetchTvSeriesCountDownLatch.countDown();
                    }
                });

    }

    public void fetchPopularTvSeries(final Context context, final int page, final NetworkCallback<FetchPopularTvSeriesResponse> callback) {
        if(!ConnectionUtil.isConnected(context)) {
            callback.onError();
            return;
        }
        if(tvSeriesGenres.isEmpty()) {
            fetchTvSeriesCountDownLatch = new CountDownLatch(1);
            fetchTvSeriesGenres(callback);
        }

        CallableExecutor.executeCallable(
                new Callable<FetchPopularTvSeriesResponse>() {
                    @Override
                    public FetchPopularTvSeriesResponse call() throws Exception {
                        fetchTvSeriesCountDownLatch.await();
                        Response<FetchPopularTvSeriesResponse> response =
                                repository.getPopularTvSeries(UrlUtil.MOVIE_DB_API_KEY, page).execute();
                        return response.body();
                    }
                },
                new NetworkCallback<FetchPopularTvSeriesResponse>() {
                    @Override
                    public void onSuccess(FetchPopularTvSeriesResponse fetchPopularTvSeriesResponse) {
                        lastLoadedTvSeriesPage = page;

                        fillGenresToTvSeries(fetchPopularTvSeriesResponse.getTvSeries());
                        tvSeriesList.addAll(fetchPopularTvSeriesResponse.getTvSeries());

                        callback.onSuccess(fetchPopularTvSeriesResponse);

                        if(tvSeriesList.size() < COUNT_TO_LOAD) {
                            fetchPopularTvSeries(context, lastLoadedTvSeriesPage + 1, callback);
                        }

                        Log.d(TAG, "Loaded " + fetchPopularTvSeriesResponse.getTvSeries().size() +
                                " tv series, last loaded page is: " + lastLoadedTvSeriesPage);
                    }

                    @Override
                    public void onError() {
                        callback.onError();
                        Log.d(TAG, "fetchPopularTvSeries - onError");
                    }
                });
    }

    private void fillGenresToTvSeries(List<TvSeries> tvSeriesList) {
        for(TvSeries tvSeries : tvSeriesList) {
            if(tvSeries.getGenreIdList() == null) return;

            List<Genre> newGenreList = new ArrayList<>();

            for(Integer genreId : tvSeries.getGenreIdList()) {
                newGenreList.add(tvSeriesGenres.get(genreId));
            }
            tvSeries.setGenres(newGenreList);
        }
    }

}
