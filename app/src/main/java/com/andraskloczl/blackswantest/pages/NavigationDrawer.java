package com.andraskloczl.blackswantest.pages;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.andraskloczl.blackswantest.MainActivity;
import com.andraskloczl.blackswantest.R;

import lombok.Getter;


public class NavigationDrawer {
    private MainActivity containingActivity;

    @Getter
    private BlackSwanMenuFragment visibleFragment;

    private ActionBarDrawerToggle drawerToggle;

    private DrawerLayout drawerLayout;

    private ListView drawerItems;

    private ArrayAdapter<BlackSwanMenuFragment> menuFragmentArrayAdapter;

    private int fragmentContainerId;

    public NavigationDrawer(final AppCompatActivity activity,
                            final Toolbar toolbar,
                            final DrawerLayout layout,
                            final ListView drawerItemsContainer,
                            final int fragmentContainerId) {
        // Keep a reference to the activity containing this navigation drawer.
        this.containingActivity = (MainActivity) activity;
        this.drawerItems = drawerItemsContainer;
        menuFragmentArrayAdapter = new ArrayAdapter<BlackSwanMenuFragment>(activity, R.layout.list_item_nav_drawer_item) {
            @Override
            public View getView(final int position, final View convertView,
                                final ViewGroup parent) {
                View view = convertView;
                if (view == null) {
                    view = activity.getLayoutInflater().inflate(R.layout.list_item_nav_drawer_item, parent, false);
                }
                final BlackSwanMenuFragment menuItem = getItem(position);
                ((TextView) view.findViewById(R.id.list_item_drawer_item_text)).setText(menuItem.getTitleResID());
                return view;
            }
        };
        drawerItems.setAdapter(menuFragmentArrayAdapter);
        drawerItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view,
                                    final int position, final long id) {
                Log.d("Navigationdrawer", "onItemClick");

                final FragmentManager fragmentManager = activity.getSupportFragmentManager();

                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                final BlackSwanMenuFragment fragment = menuFragmentArrayAdapter.getItem(position);

                showMenuFragment(fragment);
            }
        });
        this.drawerLayout = layout;
        this.fragmentContainerId = fragmentContainerId;

        drawerToggle = new ActionBarDrawerToggle(activity, drawerLayout, toolbar,
                R.string.app_name, R.string.app_name) {

            @Override
            public void syncState() {
                super.syncState();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                drawerView.bringToFront();
                drawerView.requestLayout();
            }
        };

        drawerLayout.addDrawerListener(drawerToggle);

        drawerToggle.syncState();
    }

    public void showMenuFragment(BlackSwanMenuFragment fragmentToShow) {

        if (visibleFragment == fragmentToShow) {
            closeDrawer();
            return;
        }

        if (visibleFragment != null) {
            containingActivity.getSupportFragmentManager().beginTransaction()
                    .hide(visibleFragment)
                    .commit();
        }

        final FragmentManager fragmentManager = containingActivity.getSupportFragmentManager();

        boolean hasAdded = fragmentManager.findFragmentByTag(fragmentToShow.getFragmentTag()) != null;
        if (!hasAdded) {
            fragmentManager.beginTransaction()
                    .add(fragmentContainerId, fragmentToShow, fragmentToShow.getFragmentTag())
                    .commit();
            visibleFragment = fragmentToShow;
        } else {
            fragmentManager.beginTransaction()
                    .show(fragmentToShow)
                    .commit();
            visibleFragment = fragmentToShow;
        }

        final ActionBar actionBar = containingActivity.getSupportActionBar();
        if (actionBar != null) {
            containingActivity.setToolbarTitle(containingActivity.getString(fragmentToShow.getTitleResID()));
        }
        closeDrawer();
    }

    public void addNewMenuFragment(BlackSwanMenuFragment menuFragment) {
        menuFragmentArrayAdapter.add(menuFragment);
        menuFragmentArrayAdapter.notifyDataSetChanged();
    }

    public void closeDrawer() {
        drawerLayout.closeDrawers();
    }

    public boolean isDrawerOpen() {
        return drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    public boolean isFirstPageVisible() {
        return visibleFragment == menuFragmentArrayAdapter.getItem(0);
    }

    public void goToFirstPage() {
        showMenuFragment(menuFragmentArrayAdapter.getItem(0));
    }
}
