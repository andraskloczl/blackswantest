package com.andraskloczl.blackswantest.pages.movies.movielist;

import com.andraskloczl.blackswantest.network.models.movie.Movie;

/**
 * Created by andraskloczl on 05/07/16.
 */
public interface MovieClickedListener {
    void onMovieClicked(Movie movie);
}
