package com.andraskloczl.blackswantest.pages;

import android.support.v4.app.Fragment;
import android.util.Log;

import com.andraskloczl.blackswantest.MainActivity;

/**
 * Created by andraskloczl on 05/07/16.
 */
public abstract class BlackSwanMenuFragment extends Fragment {
    protected void log(String logtext) {
        Log.d(this.getClass().getSimpleName(), logtext);
    }

    public String getFragmentTag() {
        return getClass().getSimpleName();
    }

    protected void onNetworkError() {
        ((MainActivity)getActivity()).onNetworkError();
    }

    public abstract void reload();

    public abstract int getTitleResID();
}
