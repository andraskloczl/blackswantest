package com.andraskloczl.blackswantest.pages.movies;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andraskloczl.blackswantest.BlackSwanApplication;
import com.andraskloczl.blackswantest.MainActivity;
import com.andraskloczl.blackswantest.R;
import com.andraskloczl.blackswantest.moviedb.MovieDbManager;
import com.andraskloczl.blackswantest.network.NetworkCallback;
import com.andraskloczl.blackswantest.network.models.movie.FetchPopularMoviesResponse;
import com.andraskloczl.blackswantest.network.models.movie.Movie;
import com.andraskloczl.blackswantest.pages.BlackSwanMenuFragment;
import com.andraskloczl.blackswantest.pages.details.DetailsActivity;
import com.andraskloczl.blackswantest.pages.movies.movielist.MovieClickedListener;
import com.andraskloczl.blackswantest.pages.movies.movielist.MovieListAdapter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by andraskloczl on 05/07/16.
 */
public class MoviesMenuFragment extends BlackSwanMenuFragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<Movie> movieList;

    @Inject
    public MovieDbManager movieDbManager;

    private MainActivity containingActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu_movies, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((BlackSwanApplication)getActivity().getApplication()).getMainComponent().inject(this);
        init();
    }

    private void init() {
        containingActivity = (MainActivity) getActivity();
        containingActivity.setVisibilityForLoadingView(true);

        recyclerView = (RecyclerView) getActivity().findViewById(R.id.fragment_menu_movies_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        movieList = movieDbManager.getMovieList();
        adapter = new MovieListAdapter(movieList,
                new MovieClickedListener() {
                    @Override
                    public void onMovieClicked(Movie movie) {
                        log("onMovieClicked - " + movie.getTitle());

                        startActivity(DetailsActivity.newIntent(getActivity(), movie));
                    }
                });
        recyclerView.setAdapter(adapter);
        loadMovies();
    }

    private void loadMovies() {
        movieDbManager.fetchPopularMovies(getActivity(),
                movieDbManager.getLastLoadedMoviePage() + 1,
                new NetworkCallback<FetchPopularMoviesResponse>() {

                    @Override
                    public void onSuccess(FetchPopularMoviesResponse fetchPopularMoviesResponse) {
                        log("loadMovies - onSuccess");
                        int count = fetchPopularMoviesResponse.getMovies().size();
                        adapter.notifyItemRangeInserted(movieList.size() - count, count);

                        containingActivity.setVisibilityForLoadingView(false);
                    }

                    @Override
                    public void onError() {
                        log("loadMovies - ERROR");
                        onNetworkError();
                    }
                });

    }

    @Override
    public void reload() {
        loadMovies();
    }

    @Override
    public int getTitleResID() {
        return R.string.top20_popular_movies;
    }
}
