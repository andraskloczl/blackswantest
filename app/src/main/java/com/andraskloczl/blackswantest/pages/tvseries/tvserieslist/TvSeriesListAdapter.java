package com.andraskloczl.blackswantest.pages.tvseries.tvserieslist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andraskloczl.blackswantest.R;
import com.andraskloczl.blackswantest.network.models.tv.TvSeries;

import java.util.List;

/**
 * Created by andraskloczl on 04/07/16.
 */
public class TvSeriesListAdapter extends RecyclerView.Adapter<TvSeriesListViewHolder> {

    private List<TvSeries> tvSeriesList;
    private TvSeriesClickedListener tvSeriesClickedListener;

    public TvSeriesListAdapter(List<TvSeries> tvSeriesList, TvSeriesClickedListener listener) {
        this.tvSeriesList = tvSeriesList;
        tvSeriesClickedListener = listener;
    }

    @Override
    public TvSeriesListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_tvseries, parent, false);
        TvSeriesListViewHolder viewHolder = new TvSeriesListViewHolder(view);
        viewHolder.getMoreInfoButton().setOnClickListener(onMoreInfoClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TvSeriesListViewHolder holder, int position) {
        final TvSeries tvSeries = tvSeriesList.get(position);
        holder.setFields(tvSeries, position);
    }

    @Override
    public int getItemCount() {
        return tvSeriesList.size();
    }

    private View.OnClickListener onMoreInfoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int position = (int) view.getTag();

            if(tvSeriesClickedListener != null) {
                tvSeriesClickedListener.onTvSeriesClicked(tvSeriesList.get(position));
            }
        }
    };


}

