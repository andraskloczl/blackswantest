package com.andraskloczl.blackswantest.pages.tvseries.tvserieslist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andraskloczl.blackswantest.R;
import com.andraskloczl.blackswantest.network.models.tv.TvSeries;
import com.andraskloczl.blackswantest.utils.GenreUtil;
import com.andraskloczl.blackswantest.utils.UrlUtil;
import com.squareup.picasso.Picasso;

import lombok.Getter;

/**
 * Created by andraskloczl on 04/07/16.
 */
public class TvSeriesListViewHolder extends RecyclerView.ViewHolder {

    private ImageView image;
    private TextView titleText;
    private TextView ratingText;
    private TextView categoriesText;
    private TextView yearText;
    private TextView descriptionText;
    @Getter
    private View moreInfoButton;

    public TvSeriesListViewHolder(View itemView) {
        super(itemView);
        initViews(itemView);
    }

    private void initViews(View view) {
        image = (ImageView) view.findViewById(R.id.list_item_tvseries_image);
        titleText = (TextView) view.findViewById(R.id.list_item_tvseries_title);
        ratingText = (TextView) view.findViewById(R.id.list_item_tvseries_rating);
        categoriesText = (TextView) view.findViewById(R.id.list_item_tvseries_categories);
        yearText = (TextView) view.findViewById(R.id.list_item_tvseries_year);
        descriptionText = (TextView) view.findViewById(R.id.list_item_tvseries_description);
        moreInfoButton = view.findViewById(R.id.list_item_tvseries_more_info);
    }

    public void setFields(TvSeries tvSeries, int position) {
        Picasso.with(image.getContext())
                .load(UrlUtil.getImageUrl(tvSeries.getPosterPath()))
                .error(R.drawable.error)
                .placeholder(R.drawable.progress_animation)
                .into(image);
        titleText.setText(tvSeries.getName());
        ratingText.setText(tvSeries.getAverageVote() + "");
        categoriesText.setText(GenreUtil.getGenresString(tvSeries.getGenres()));
        yearText.setText(tvSeries.getFirstAirDate().substring(0, 4));
        descriptionText.setText(tvSeries.getOverview());

        moreInfoButton.setTag(position);
    }
}
