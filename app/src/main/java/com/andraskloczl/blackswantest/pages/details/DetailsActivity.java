package com.andraskloczl.blackswantest.pages.details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.andraskloczl.blackswantest.R;
import com.andraskloczl.blackswantest.network.models.movie.Movie;
import com.andraskloczl.blackswantest.network.models.tv.TvSeries;
import com.andraskloczl.blackswantest.utils.GenreUtil;
import com.andraskloczl.blackswantest.utils.UrlUtil;
import com.squareup.picasso.Picasso;

/**
 * Created by andraskloczl on 05/07/16.
 */
public class DetailsActivity extends AppCompatActivity{
    private static final String MOVIE_KEY = "movie";
    private static final String TV_SERIES_KEY = "tv_series";

    private Movie movie;
    private TvSeries tvSeries;

    private Toolbar toolbar;

    private ImageView imageView;
    private TextView titleTextView;
    private TextView categoriesTextView;
    private TextView ratingTextView;
    private TextView dateTextView;
    private TextView descriptionTextView;

    private TextView toolbarTitle;

    public static Intent newIntent(Context context, Movie movie) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(MOVIE_KEY, movie);
        return intent;
    }

    public static Intent newIntent(Context context, TvSeries tvSeries) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(TV_SERIES_KEY, tvSeries);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_from_right_to_center, R.anim.slide_from_center_to_left);
        setContentView(R.layout.activity_details);
        init();
    }

    private void init() {
        movie = (Movie) getIntent().getSerializableExtra(MOVIE_KEY);
        tvSeries = (TvSeries) getIntent().getSerializableExtra(TV_SERIES_KEY);
        initViews();
    }

    private void initViews() {
        imageView = (ImageView) findViewById(R.id.activity_movie_details_image);
        titleTextView = (TextView) findViewById(R.id.activity_movie_details_title);
        categoriesTextView = (TextView) findViewById(R.id.activity_movie_details_categories);
        ratingTextView = (TextView) findViewById(R.id.activity_movie_details_rating);
        dateTextView = (TextView) findViewById(R.id.activity_movie_details_date);
        descriptionTextView = (TextView) findViewById(R.id.activity_movie_details_description);

        toolbarTitle = (TextView) findViewById(R.id.blackswan_toolbar_title);


        String imagePath = null;

        if(movie != null) {
            toolbarTitle.setText(movie.getTitle());
            imagePath = movie.getPosterPath();
            titleTextView.setText(movie.getTitle());
            categoriesTextView.setText(GenreUtil.getGenresString(movie.getGenres()));
            ratingTextView.setText(movie.getAverageVote() + "");
            dateTextView.setText(movie.getReleaseDateText());
            descriptionTextView.setText(movie.getOverview());
        } else if(tvSeries != null) {
            toolbarTitle.setText(tvSeries.getName());
            imagePath = tvSeries.getPosterPath();
            titleTextView.setText(tvSeries.getName());
            categoriesTextView.setText(GenreUtil.getGenresString(tvSeries.getGenres()));
            ratingTextView.setText(tvSeries.getAverageVote() + "");
            dateTextView.setText(tvSeries.getFirstAirDate());
            descriptionTextView.setText(tvSeries.getOverview());
        }

        Picasso.with(this)
                .load(UrlUtil.getImageUrl(imagePath))
                .placeholder(R.drawable.progress_animation)
                .error(R.drawable.error)
                .into(imageView);


    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_from_left_to_center, R.anim.slide_from_center_to_right);
    }
}
