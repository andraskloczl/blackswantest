package com.andraskloczl.blackswantest.pages.tvseries;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andraskloczl.blackswantest.BlackSwanApplication;
import com.andraskloczl.blackswantest.MainActivity;
import com.andraskloczl.blackswantest.R;
import com.andraskloczl.blackswantest.moviedb.MovieDbManager;
import com.andraskloczl.blackswantest.network.NetworkCallback;
import com.andraskloczl.blackswantest.network.models.tv.FetchPopularTvSeriesResponse;
import com.andraskloczl.blackswantest.network.models.tv.TvSeries;
import com.andraskloczl.blackswantest.pages.BlackSwanMenuFragment;
import com.andraskloczl.blackswantest.pages.details.DetailsActivity;
import com.andraskloczl.blackswantest.pages.tvseries.tvserieslist.TvSeriesClickedListener;
import com.andraskloczl.blackswantest.pages.tvseries.tvserieslist.TvSeriesListAdapter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by andraskloczl on 05/07/16.
 */
public class TvSeriesMenuFragment extends BlackSwanMenuFragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<TvSeries> tvSeriesList;

    @Inject
    public MovieDbManager movieDbManager;

    private MainActivity containingActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu_tvseries, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((BlackSwanApplication) getActivity().getApplication()).getMainComponent().inject(this);
        init();
    }

    private void init() {
        containingActivity = (MainActivity) getActivity();
        containingActivity.setVisibilityForLoadingView(true);

        recyclerView = (RecyclerView) getActivity().findViewById(R.id.fragment_menu_tvseries_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvSeriesList = movieDbManager.getTvSeriesList();
        adapter = new TvSeriesListAdapter(tvSeriesList,
                new TvSeriesClickedListener() {
                    @Override
                    public void onTvSeriesClicked(TvSeries tvSeries) {
                        log("onTvSeriesclicked - " + tvSeries.getName());
                        startActivity(DetailsActivity.newIntent(getActivity(), tvSeries));
                    }
                });
        recyclerView.setAdapter(adapter);
        loadTvSeries();
    }

    private void loadTvSeries() {
        movieDbManager.fetchPopularTvSeries(getActivity(),
                movieDbManager.getLastLoadedTvSeriesPage() + 1,
                new NetworkCallback<FetchPopularTvSeriesResponse>() {

                    @Override
                    public void onSuccess(FetchPopularTvSeriesResponse fetchPopularTvSeriesResponse) {
                        log("loadTvSeries - onSuccess");
                        int count = fetchPopularTvSeriesResponse.getTvSeries().size();
                        adapter.notifyItemRangeInserted(tvSeriesList.size() - count, count);

                        containingActivity.setVisibilityForLoadingView(false);
                    }

                    @Override
                    public void onError() {
                        log("loadTvSeries - ERROR");
                        onNetworkError();
                    }
                });

    }

    @Override
    public void reload() {
        loadTvSeries();
    }

    @Override
    public int getTitleResID() {
        return R.string.top20_popular_tv_series;
    }
}
