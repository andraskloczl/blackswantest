package com.andraskloczl.blackswantest.pages.movies.movielist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andraskloczl.blackswantest.R;
import com.andraskloczl.blackswantest.network.models.movie.Movie;
import com.andraskloczl.blackswantest.utils.GenreUtil;
import com.andraskloczl.blackswantest.utils.UrlUtil;
import com.squareup.picasso.Picasso;

import lombok.Getter;

/**
 * Created by andraskloczl on 04/07/16.
 */
public class MovieListViewHolder extends RecyclerView.ViewHolder {

    private ImageView movieImage;
    private TextView titleText;
    private TextView ratingText;
    private TextView categoriesText;
    private TextView yearText;
    private TextView descriptionText;
    @Getter
    private View moreInfoButton;

    public MovieListViewHolder(View itemView) {
        super(itemView);
        initViews(itemView);
    }

    private void initViews(View view) {
        movieImage = (ImageView) view.findViewById(R.id.list_item_movie_image);
        titleText = (TextView) view.findViewById(R.id.list_item_movie_title);
        ratingText = (TextView) view.findViewById(R.id.list_item_movie_rating);
        categoriesText = (TextView) view.findViewById(R.id.list_item_movie_categories);
        yearText = (TextView) view.findViewById(R.id.list_item_movie_year);
        descriptionText = (TextView) view.findViewById(R.id.list_item_movie_description);
        moreInfoButton = view.findViewById(R.id.list_item_movie_more_info);
    }

    public void setFields(Movie movie, int position) {
        Picasso.with(movieImage.getContext())
                .load(UrlUtil.getImageUrl(movie.getPosterPath()))
                .error(R.drawable.error)
                .placeholder(R.drawable.progress_animation)
                .into(movieImage);
        titleText.setText(movie.getTitle());
        ratingText.setText(movie.getAverageVote() + "");
        categoriesText.setText(GenreUtil.getGenresString(movie.getGenres()));
        yearText.setText(movie.getReleaseDateText().substring(0, 4));
        descriptionText.setText(movie.getOverview());

        moreInfoButton.setTag(position);
    }
}
