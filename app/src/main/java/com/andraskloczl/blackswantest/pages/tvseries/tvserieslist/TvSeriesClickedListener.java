package com.andraskloczl.blackswantest.pages.tvseries.tvserieslist;

import com.andraskloczl.blackswantest.network.models.tv.TvSeries;

/**
 * Created by andraskloczl on 05/07/16.
 */
public interface TvSeriesClickedListener {
    void onTvSeriesClicked(TvSeries tvSeries);
}
