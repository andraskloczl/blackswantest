package com.andraskloczl.blackswantest.pages.movies.movielist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andraskloczl.blackswantest.R;
import com.andraskloczl.blackswantest.network.models.movie.Movie;

import java.util.List;

/**
 * Created by andraskloczl on 04/07/16.
 */
public class MovieListAdapter extends RecyclerView.Adapter<MovieListViewHolder> {

    private List<Movie> movieList;
    private MovieClickedListener movieClickedListener;

    public MovieListAdapter(List<Movie> movies, MovieClickedListener listener) {
        movieList = movies;
        movieClickedListener = listener;
    }

    @Override
    public MovieListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_movie, parent, false);
        MovieListViewHolder viewHolder = new MovieListViewHolder(view);
        viewHolder.getMoreInfoButton().setOnClickListener(onMoreInfoClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MovieListViewHolder holder, int position) {
        final Movie movie = movieList.get(position);
        holder.setFields(movie, position);
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    private View.OnClickListener onMoreInfoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int position = (int) view.getTag();

            if(movieClickedListener != null) {
                movieClickedListener.onMovieClicked(movieList.get(position));
            }
        }
    };


}
